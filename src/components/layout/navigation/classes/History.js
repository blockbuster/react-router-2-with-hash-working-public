/**
 * Created by griga on 12/23/15.
 */

import {createHashHistory} from 'history'
export default createHashHistory({queryKey: false})
