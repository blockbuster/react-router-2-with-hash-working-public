ExUnit.start

Mix.Task.run "ecto.create", ~w(-r DummyStartedByPhoenix.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r DummyStartedByPhoenix.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(DummyStartedByPhoenix.Repo)

