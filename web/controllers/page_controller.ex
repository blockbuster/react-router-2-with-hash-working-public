defmodule DummyStartedByPhoenix.PageController do
  use DummyStartedByPhoenix.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def account(conn, _params) do
    render conn, "account.html",
      layout: {DummyStartedByPhoenix.LayoutView, "account.html"}
  end
end
